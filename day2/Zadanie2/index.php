<?php
interface iDB {
    /**
     * Wykonuje zapytanie, zapisuje uchwyt
     * @param string $query Zapytanie do wykonania
     * @return bool Czy się udało wykonać zapytanie czy nie
     */
    public function query($query);

    /**
     * Zwraca liczbę wierszy zmodyfikowanych przez ostatnie zapytanie
     * @return int
     */
    public function getAffectedRows();

    /**
     * Zwraca jeden (kolejny) wiersz z wyniku ostatniego zapytania
     * @return mixed
     */
    public function getRow();

    /**
     * Zwraca wszystkie wiersze z wyniku ostatniego zapytania
     * @return mixed
     */
    public function getAllRows();
}

class DB implements iDB {
	public $mysqli;
	public $query;
	
	public function __construct($host, $login, $password, $dbName) {
		$this->mysqli = mysqli_connect($host, $login, $password, $dbName);
	}

    public function query($query) {
		$this->query = mysqli_query($this->mysqli, $query);
	}

    public function getAffectedRows() {
		return mysqli_affected_rows($this->mysqli);
	}

    public function getRow() {
		return mysqli_fetch_row($this->query);
	}

    public function getAllRows() {
		$results = array();
        while (($row = $this->getRow())) {
            $results[] = $row;
        }
        return $results;
	}
}

$baza = new DB('127.0.0.1', 'root', '', 'phpcamp_kdrzewicz');
?>