﻿<?php

require_once("BaseProduct.php");

class VirtualProduct extends BaseProduct {

	public function __call($name, $arguments) {
		if(substr($name, 0, 3) == 'set') {
			$arg = strtolower(substr($name, 3));
			$this->$arg = $arguments[0];
		}
		else if(substr($name, 0, 3) == 'get') {
			return $this->strtolower(substr($name, 3));
		}
	}
}
?>