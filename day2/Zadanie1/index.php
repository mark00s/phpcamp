﻿<?php

function prePrint($wartosc) {
	print_r("<pre>");
	print_r($wartosc);
	print_r("</pre>");
}

require_once("lib/Product.php");
require_once("lib/VirtualProduct.php");

$producent = ['Amazon', 'Helion', 'IAI', 'Microsoft', 'AM Szczecin'];

$time_pre = microtime(true);
for($i = 0; $i < 1; $i++) {
	$product[$i] = new Product();
		
	$product[$i]->id = $i+1; 
	$product[$i]->name = "Produkt ".($i+1); 
	$product[$i]->price = rand(1, 10000)/100;
	$product[$i]->currency = "PLN";
	$product[$i]->quantity = rand(1, 100);
	$product[$i]->images = "images/product".($i+1).".png";
	$product[$i]->manufacturer = $producent[rand(0, 4)];
	$product[$i]->id_categories = [1, 2];
	$product[$i]->weight = rand(1, 50)/10;
}
$time_post = microtime(true);

echo $time_post - $time_pre;

echo "<br>";

$memoryUsed = memory_get_peak_usage()/1024;
echo $memoryUsed;

$product1 = new VirtualProduct();
		
	$product1->setId($i+1); 
	$product1->setName("Produkt ".($i+1)); 
	$product1->setPrice(rand(1, 10000)/100);
	$product1->setCurrency("PLN");
	$product1->setQuantity(rand(1, 100));
	$product1->setImages("images/product".($i+1).".png");
	$product1->setManufacturer($producent[rand(0, 4)]);
	$product1->setId_categories([1, 2]);
	$product1->setAttachment(rand(1, 50)/10);
	
$product[] = $product1;

prePrint($product);
?>